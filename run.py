import mayruq as uq
import dill   as pkl

model = uq.MayrUQ()

model.optimize_bootstrapped(n_samples=10000, weighting=True, monitoring=True, parallel=True)

pkl.dump(model, open('model.pkl', 'wb'))
